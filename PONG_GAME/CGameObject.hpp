//
//  CGameObject.hpp
//  PONG_GAME
//
//  Created by Domi on 01/11/2018.
//  Copyright © 2018 Krisu. All rights reserved.
//

#ifndef CGameObject_hpp
#define CGameObject_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>

struct Position
{
    int Pos_X;
    int Pos_Y;
    
    Position(int posX,int posY)
    {
        Pos_X = posX;
        Pos_Y = posY;
    }
};

class CGameObject
{
public:
    CGameObject();
    
    sf::Vector2f GetPosition();
    

    
    virtual void Update(float dt);
    
protected:
    //Position position = Position(0,0);
    
    sf::Vector2f position = sf::Vector2f(0,0);
    
    static int instanceID;
    
private:
    
};

#endif /* CGameObject_hpp */
