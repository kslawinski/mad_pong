//
//  CBall.hpp
//  PONG_GAME
//
//  Created by Domi on 01/11/2018.
//  Copyright © 2018 Krisu. All rights reserved.
//

#ifndef CBall_hpp
#define CBall_hpp

#include <stdio.h>
#include "CGameObject.hpp"

class Cball : public CGameObject
{
public:
    Cball();
    Cball(sf::Texture* ballTexture, sf::Sprite* paddle);
    
    virtual void Update(float dt) override;
    
    sf::Sprite* GetSprite();
    
    bool CheckCollision(sf::Vector2f& outNormal);
private:
    sf::Sprite* ballSprite;
    
    sf::Vector2f direction;
    sf::Vector2f velocity;
    float speed;
    
    sf::Sprite* paddle;
};

#endif /* CBall_hpp */
