//
//  Utills.h
//  PONG_GAME
//
//  Created by Domi on 04/11/2018.
//  Copyright © 2018 Krisu. All rights reserved.
//

#ifndef Utills_h
#define Utills_h

#include <SFML/Graphics.hpp>
#include <math.h>       /* sqrt */
#include <algorithm>

#define UP 0,-1
#define DOWN 0,1
#define RIGHT 1,0
#define LEFT -1,0

static float GetDot(sf::Vector2f vecA,sf::Vector2f vecB);

static sf::Vector2f GetReflection(sf::Vector2f inVec, sf::Vector2f normal);

static sf::Vector2f Normalize( sf::Vector2f source);

static sf::Vector2f ClampPosition(sf::Vector2f min, sf::Vector2f max, sf::Vector2f inPos);

float GetDot(sf::Vector2f vecA,sf::Vector2f vecB)
{
    float dot = 0;
    
    dot = (vecA.x * vecB.x) + (vecA.y * vecB.y);
    
    return dot;
}

static sf::Vector2f GetReflection(sf::Vector2f inVec, sf::Vector2f normal)
{
    sf::Vector2f reflection;
    
    //r=d−2(d⋅n)n
    
    //out = incidentVec - 2.f * Dot(incidentVec, normal) * normal;
    
    reflection = (inVec - sf::Vector2f(2.0f * (GetDot(inVec, normal) * normal)));
    
    return Normalize(reflection);
}

static sf::Vector2f Normalize( sf::Vector2f source)
{
    // divide a vector's components by the vector's lenth
    float length = sqrt((source.x * source.x) + (source.y * source.y));
    if (length != 0)
        return sf::Vector2f(source.x / length, source.y / length);
    else
        return source;
}

static sf::Vector2f ClampPosition(sf::Vector2f min, sf::Vector2f max, sf::Vector2f inPos)
{
    sf::Vector2f outPosition;
    
    if(inPos.x > max.x)
        outPosition.x = max.x;
    else if (inPos.x < min.x)
        outPosition.x = min.x;
    else
        outPosition.x = inPos.x;
    
    if(inPos.y > max.y)
        outPosition.y = max.y;
    else if(inPos.y < min.y)
        inPos.y = min.y;
    else
        outPosition.y = inPos.y;
    
    return outPosition;
}

#endif /* Utills_h */
