//
//  CGameObject.cpp
//  PONG_GAME
//
//  Created by Domi on 01/11/2018.
//  Copyright © 2018 Krisu. All rights reserved.
//

#include "CGameObject.hpp"

CGameObject::CGameObject()
{
    position = sf::Vector2f(0,0); // reset position

}

void CGameObject::Update(float dt)
{
    
}

sf::Vector2f CGameObject::GetPosition()
{
    return position;
}
