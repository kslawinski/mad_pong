//
//  main.cpp
//  PONG_GAME
//
//  Created by Domi on 31/10/2018.
//  Copyright © 2018 Krisu. All rights reserved.
//

#include <iostream>

#include <SFML/Graphics.hpp>
#include "CBall.hpp"
#include "CPaddle.hpp"

#include "Utills.h"

int main(int argc, const char * argv[]) {
    
    sf::Clock clock = sf::Clock();
    sf::Time dt = sf::Time();
    float deltaTime = 0;
    
    sf::Texture ballTexture;
    sf::Texture paddleTexture;
    
    
    // LOAD RESOURCES
    if(!ballTexture.loadFromFile("ball2.png"))
    {
        std::cout << "CANT LOAD FILE!! ball.png" << std::endl;
    }
    
    if(!paddleTexture.loadFromFile("paddle.png"))
    {
        std::cout << "CANT LOAD FILE!! paddle.png" << std::endl;
    }
    
    sf::RenderWindow window(sf::VideoMode(640,480), "SFML ON MAC TEST");
    

    
    CPaddle paddle = CPaddle(&paddleTexture);
    
    sf::Sprite* sprite = paddle.GetSprite();
    
    Cball ball = Cball(&ballTexture, sprite);
    
    while (window.isOpen())
    {
        
        sf::Event event;
        
        while (window.pollEvent(event))
        {
            switch (event.type) {
                case sf::Event::Closed:
                    window.close();
                    break;
                    
                default:
                    break;
            }
            
        }
        
        // GET INPUTS
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            paddle.MovePaddle(-1);
            std::cout << "MOVE LEFT" << std::endl;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            paddle.MovePaddle(1);
            std::cout << "MOVE RIGHT" << std::endl;
        }
        
        ball.Update(dt.asSeconds());
        paddle.Update(dt.asSeconds());
        
        //float dot = Utills_h::GetDot(sf::Vector2f(UP), sf::Vector2f(0,1));
        
        //std::cout <<  "Dot: " << dot << std::endl;
        
        window.clear();
        
        
        // draw objects here
        window.draw(*ball.GetSprite());
        window.draw(*paddle.GetSprite());
        
        window.display();
        
        //deltaTime = (float)clock.restart().asSeconds();
        dt = clock.restart();
    }
}


