//
//  CPaddle.hpp
//  PONG_GAME
//
//  Created by Domi on 06/11/2018.
//  Copyright © 2018 Krisu. All rights reserved.
//

#ifndef CPaddle_hpp
#define CPaddle_hpp

#include <stdio.h>
#include "CGameObject.hpp"

class CPaddle : public CGameObject
{
public:
    CPaddle(sf::Texture* paddleTexture);
    
    virtual void Update(float dt) override;
    
    sf::Sprite* GetSprite();
    
    void MovePaddle(int Direction);
    
private:
    sf::Sprite* paddleSprite;
    
    sf::Vector2f direction;
    sf::Vector2f velocity;
    float speed;
};

#endif /* CPaddle_hpp */
