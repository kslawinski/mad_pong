//
//  CPaddle.cpp
//  PONG_GAME
//
//  Created by Domi on 06/11/2018.
//  Copyright © 2018 Krisu. All rights reserved.
//

#include "CPaddle.hpp"
#include "Utills.h"

CPaddle::CPaddle(sf::Texture* paddleTexture)
{
    //CREATE SPRITE AND SET TEXTURE
    if(paddleTexture)
    {
        paddleSprite = new sf::Sprite();
        paddleSprite->setTexture(*paddleTexture);
    }
    
    position = sf::Vector2f(640/2,480 - 80);
    paddleSprite->setPosition(position.x, position.y);
    
    speed = 300;
}

void CPaddle::Update(float dt)
{
    CGameObject::Update(dt);
    
    velocity = direction * speed * dt; // calculate velocity
    
    sf::Vector2f newPos = position + velocity;
    if(newPos.x >=  (640 - paddleSprite->getGlobalBounds().width))
    {
        newPos.x = 640 - paddleSprite->getGlobalBounds().width;
    }
    else if(newPos.x < 0)
    {
        newPos.x = 0;
    }
    
    position = newPos;
    paddleSprite->setPosition(position.x, position.y);
    
    direction = sf::Vector2f(0,0);
}

sf::Sprite* CPaddle::GetSprite()
{
    return paddleSprite;
}

void CPaddle::MovePaddle(int Direction)
{
    //if((position.x > 0 && Direction < 0) || (Direction > 0 && position.x < 640 - paddleSprite->getGlobalBounds().width))

        direction = sf::Vector2f(Direction,0);

        //position += sf::Vector2f(Direction * speed,0);
    
    //paddleSprite->setPosition(position.x, position.y);
}
