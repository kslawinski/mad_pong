//
//  CBall.cpp
//  PONG_GAME
//
//  Created by Domi on 01/11/2018.
//  Copyright © 2018 Krisu. All rights reserved.
//

#include "CBall.hpp"
#include <iostream>
#include "Utills.h"

Cball::Cball(sf::Texture* ballTexture, sf::Sprite* paddle)
{
    direction = sf::Vector2f(1,1);
    velocity = sf::Vector2f(0,0);
    speed = 300.0f;
    
    if(ballTexture)
        ballSprite = new sf::Sprite();
    ballSprite->setTexture(*ballTexture);
    
    this->paddle = paddle;
}

void Cball::Update(float dt)
{
    CGameObject::Update(dt);

    sf::Vector2f normal;
    
    if(CheckCollision(normal))
    {
        direction = Utills_h::GetReflection(Utills_h::Normalize(velocity),normal);//-direction;
    }
    
    velocity = direction * speed * dt; // calculate velocity
    
    sf::Vector2f newPos = position + velocity;
    newPos = Utills_h::ClampPosition(sf::Vector2f(-10,-10), sf::Vector2f (640,480), newPos);
        std::cout << newPos.x << "   " << newPos.y << std::endl;
    position = newPos;
    
    ballSprite->setPosition(position.x, position.y);
}

sf::Sprite* Cball::GetSprite()
{
    return ballSprite;
}

bool Cball::CheckCollision(sf::Vector2f& outNormal)
{
    bool isColliding = false;
    
    auto bounds = ballSprite->getLocalBounds();
    
    if(position.x + bounds.width >= 640 || position.x < 0)
    {
        isColliding = true;
        outNormal = sf::Vector2f(LEFT);
    }
    else if(position.y + bounds.top >= 480 || position.y < 0)
    {
        isColliding = true;
        outNormal = sf::Vector2f(UP);
    }
    
    if(paddle)
    {
        //if(this->position.x < paddle->GetPosition().x  && this->position.x < (paddle->GetPosition().x + 100) && this->position.y > paddle->GetPosition().y )
        
        auto rect = paddle->getGlobalBounds();
        auto rectThis = this->ballSprite->getGlobalBounds();
        
        if(rectThis.intersects(rect))
        {
            if(this->GetSprite()->getPosition().x > (paddle->getPosition().x) && this->GetSprite()->getPosition().x <  paddle->getPosition().x + rect.width)
            {
                outNormal = sf::Vector2f(UP);
            }
            else
               direction = -direction;
            
            isColliding = true;
        }
    }
        
    return isColliding;
}
